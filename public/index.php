
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Candy 95 | Online Housing Fair</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <script src="js/vendor/modernizr.js"></script>
    <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
    <script type="text/javascript">//<![CDATA[
                //
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-933118-10']);
                _gaq.push(['_setDomainName', 'candy95.com']);
                _gaq.push(['_trackPageview']);
                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
                //]]></script>
  </head>
  <body>

    <div class="row">
      <div class="large-12 columns head-banner">
        <h1 class="text-center">The Candy 95 Online Housing Fair</h1>
      </div>
    </div>
    <div class="row property-logos">
      <div class="large-12 columns">
        <div class="row">
            <div class="large-6 medium-6 columns property holleman-crossing">
                <a href="#" data-reveal-id="Holleman-Crossing-modal"><img src="/img/Holleman-Crossing-Logo-Web-Transparent-White.png" title="Click to Find out more about Holleman Crossing"></a>
                <div id="Holleman-Crossing-modal" class="reveal-modal" data-reveal>
                    <div class="row">
                        <h2 class="left large-6 columns"><a href="http://www.hollemancrossing.com/" target="_blank">Holleman Crossing</a></h2><a href="#" class="close-reveal-modal">X</a>
                        <div class="right large-6 columns location">
                            <address>Address:<a href="https://www.google.com/maps/place/Holleman+Crossing+Apartments/@30.5845321,-96.3397497,17z/data=!3m1!4b1!4m2!3m1!1s0x864683696dffd17b:0x54ee9f7b1d91e313" target="_blank">12200 Cottage Ln<br>
                            College Station, TX 77845</a></address>
                            <phone>Phone: <a href="tel:+18444290628">1 (844) 429-0628</a></phone>
                            <p><a href="http://www.hollemancrossing.com/" target="_blank">HollemanCrossing.com</a></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <a href="http://www.hollemancrossing.com/" target="_blank"><img class="right large-6 medium-12 columns" src="/img/Holleman-Crossing-Logo-Web-Transparent.png"></a>
                        <div class="large-5 medium-6 columns">
                            <p>Holleman Crossing understands that the needs of a college student are unique. As important as it is for you to be able to find some quiet time in order to focus on your studies, they think it’s equally important that you find yourself in a community you’re proud to be a part of. What it means to be a college student was at the forefront of their minds when they created Holleman Crossing, and they’re confident that your experience while at TAMU will benefit from it.</p>

                            <strong>Community Features</strong>
                            <ul>
                                <li>Spacious, Modern Clubhouse</li>
                                <li>24 Hour Access Common Learning Areas & Cozy Study Nooks with Free WIFI / Printer Stations</li>
                                <li>Resident Social Events</li>
                                <li>Resort-Style Pool</li>
                                <li>24 Hour Access Billiards and Table-Tennis</li>
                                <li>Picnic Area with BBQ Grills</li>
                                <li>24 Hour Access State-of-the-Art Fitness Facility</li>
                                <li>Attractive Landscaping & Lush Green Spaces</li>
                            </ul>
                            <strong>Apartment Features</strong>
                            <ul>
                                <li>Fully Furnished</li>
                                <li>Free Cable / Premium Internet in Bedrooms and Living Area</li>
                                <li>Granite Counter Tops in Kitchens & Bathrooms</li>
                                <li>High Quality Black Appliances</li>
                                <li>Full-Size Washer/Dryers Included</li>
                                <li>Walk-In Closets</li>
                                <li>Wood-style Flooring in Living Area</li>
                                <li>Plush Carpeting in Bedrooms</li>
                                <li>Built-In Shelves</li>
                            </ul>
                        </div>
                        <div class="right large-6 medium-6 columns">
                            <hr>
                            <ul class="clearing-thumbs modal-gallery" data-clearing>
                              <?php
                              for ($i = 1; $i <= 13; $i++) {
                                  echo '<li><a href="/img/properties/2016-holleman-crossing-' . sprintf('%02d', $i) . '.jpg"><img src="/img/properties/2016-holleman-crossing-' . sprintf('%02d', $i) . '-th.jpg"></a></li>';
                              } ?>
                            </ul>
                        </div>
                    </div>
                    <!--<div class='embed-container'><iframe src='http://player.vimeo.com/video/100176087' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>-->
                </div>
            </div>
            <div class="large-6 medium-6 columns property junction">
                <a href="#" data-reveal-id="Junction-modal"><img src="/img/Junction-Logo-Web-Transparent-White.png" title="Click to Find out more about The Junction"></a>
                <div id="Junction-modal" class="reveal-modal large" data-reveal>
                    <div class="row">
                        <h2 class="left large-6 columns"><a href="http://thejunctionatcollegestation.com/" target="_blank">The Junction at College Station</a></h2><a href="#" class="close-reveal-modal">X</a>
                        <div class="right large-6 columns location">
                            <address>Address:<a href="https://www.google.com/maps/place/The+Junction/@30.5846709,-96.3365421,17z/data=!3m1!4b1!4m2!3m1!1s0x864683688d884cd1:0xedfb8595fdc109a5" target="_blank">1147 Holleman Drive<br>
                            College Station, TX 77845</a></address>
                            <phone>Phone: <a href="tel:+19797219160">(979) 721-9160</a></phone>
                            <p><a href="http://thejunctionatcollegestation.com/" target="_blank">TheJunctionAtCollegeStation.com</a></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <a href="http://thejunctionatcollegestation.com/" target="_blank"><img class="right large-6 medium-12 columns" src="/img/Junction-Logo-Web-Transparent.png"></a>
                        <div class="left large-5 medium-6 columns">
                            <p><strong>Brand new student cottages opening August 2016</strong></p>
                            <ul>
                                <li>3, 4, and 5 bedroom cottages, townhomes, and manor homes</li>
                                <li>On the TAMU Bus Route</li>
                                <li>Less than 2 miles from campus</li>
                                <li>Pet friendly</li>
                                <li>Roommate matching available</li>
                            </ul>

                            <p><strong>For the students that like to study</strong></p>
                            <ul>
                                <li>Multiple study rooms</li>
                                <li>Free printing</li>
                                <li>Computer lab</li>
                                <li>Free Wi-fi</li>
                            </ul>

                            <p><strong>For those that like to be more social</strong></p>
                            <ul>
                                <li>Vegas style pool with cabanas and tanning ledges</li>
                                <li>Pool house with multiple flat screens, arcade games, and pool , ping pong, and shuffle board tables</li>
                            </ul>

                            <p><strong>For those that are into fitness</strong></p>
                            <ul>
                                <li>State of the art fitness center</li>
                                <li>Cardio and multi-function strength training rooms</li>
                                <li>Saunas and steamrooms</li>
                            </ul>
                        </div>
                        <div class="right large-6 medium-6 columns">
                            <hr>
                            <ul class="clearing-thumbs modal-gallery" data-clearing>
                              <?php
                              for ($i = 1; $i <= 15; $i++) {
                                  echo '<li><a href="/img/properties/2016-junction-' . sprintf('%02d', $i) . '.jpg"><img src="/img/properties/2016-junction-' . sprintf('%02d', $i) . '-th.jpg"></a></li>';
                              } ?>
                            </ul>
                            <hr>
                            <div class="embed-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/vppY-gyUq3k" frameborder="0" allowfullscreen></iframe></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="large-6 medium-6 columns property northpoint-crossing">
                <a href="#" data-reveal-id="Northpoint-Crossing-modal"><img src="/img/Northpoint-Crossing-Logo-Web-Transparent-White.png" title="Click to Find out more about Northpoint Crossing"></a>
                <div id="Northpoint-Crossing-modal" class="reveal-modal" data-reveal>
                    <div class="row">
                        <h2 class="left large-6 columns"><a href="http://northpoint-crossing.com/" target="_blank">Northpoint Crossing</a></h2><a href="#" class="close-reveal-modal">X</a>
                        <div class="right large-6 columns location">
                            <address>Address: 1501 Northpoint Lane<br>
                            College Station, TX 77840</address>
                            <phone>Phone: <a href="tel:+19797036449">(979) 703-6449</a></phone>
                            <p><a href="http://northpoint-crossing.com/" target="_blank">Northpoint-Crossing.com</a></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <a href="http://northpoint-crossing.com" target="_blank"><img class="right large-6 medium-12 columns" src="/img/Northpoint-Crossing-Logo-Web-Transparent.png"></a>
                        <div class="large-5 medium-6 columns">
                        <p><strong>Property Features</strong></p>
                            <ul>
                                <li>One, Two, Three, & Four Bedroom Options</li>
                                <li>Individual Leases</li>
                                <li>Roommate Matching Available</li>
                                <li>Cable & Internet Included</li>
                                <li>Less Than 1 Miles From The Heart of Campus</li>
                                <li>Swimming Pool (with a 2nd coming in Phase 2!)</li>
                                <li>Rooftop Terraces</li>
                                <li>Computer Lab/Business Center</li>
                                <li>Workout Facility/Weight Room</li>
                                <li>Aerobics/Multi-Purpose Room (Phase 2)</li>
                                <li>Sauna</li>
                                <li>Steam Room</li>
                                <li>Outdoor Grill/Fireplace</li>
                                <li>Onsite Management</li>
                                <li>24 Hour Maintenance</li>
                                <li>Retail on the first level (Coming Spring ’15)</li>
                                <li>Regularly Planned Events, Activities & Services For Residents</li>
                                <li>Study Areas</li>
                                <li>Lounges</li>
                                <li>Game Rooms</li>
                                <li>Convenient To Campus</li>
                                <li>Bike Racks</li>
                            </ul>

                            <p><strong>Unit Features</strong></p>
                            <ul>
                                <li>Private Bedrooms/Bathrooms</li>
                                <li>Kitchen: Range, Refrigerator w/ ice maker, Dishwasher, Microwave</li>
                                <li>Fully Furnished</li>
                                <li>Carpeted Bedrooms</li>
                                <li>Wood Vinyl Flooring Throughout The Kitchen and Living Area</li>
                                <li>Washer/Dryer In Every Unit</li>
                                <li>High Speed Internet</li>
                                <li>Both Wireless and Hardwire</li>
                                <li>Cable In Both Living Area and Bedrooms</li>
                                <li>Central Heating and Air</li>
                                <li>Walk-In Closets</li>
                                <li>Window Blinds</li>
                            </ul>
                        </div>
                        <div class="right large-6 medium-6 columns">
                            <hr>
                            <ul class="clearing-thumbs modal-gallery" data-clearing>
                              <?php
                              for ($i = 1; $i <= 6; $i++) {
                                  echo '<li><a href="/img/properties/2016-northpoint-crossing-' . sprintf('%02d', $i) . '.jpg"><img src="/img/properties/2016-northpoint-crossing-' . sprintf('%02d', $i) . '-th.jpg"></a></li>';
                              } ?>
                            </ul>
                        </div>
                        <div class="right large-6 medium-6 columns">
                        <hr>
                            <div class="embed-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/J3AD6vwalLg" frameborder="0" allowfullscreen></iframe></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="large-6 medium-6 columns property rise">
                <a href="#" data-reveal-id="Rise-modal"><img src="/img/Rise-Logo-Web-Transparent-White.png" title="Click to Find out more about The Rise at Northgate"></a>
                <div id="Rise-modal" class="reveal-modal" data-reveal>
                    <div class="row">
                        <h2 class="left large-6 columns"><a href="http://riseatnorthgate.com" target="_blank">The Rise at Northgate</a></h2><a href="#" class="close-reveal-modal">X</a>
                        <div class="right large-6 columns location">
                            <address>Address:<a href="https://www.google.com/maps/place/Rise+At+Northgate+-+Leasing+Office/@30.621377,-96.34273,17z/data=!3m1!4b1!4m2!3m1!1s0x864683bdded8a6bb:0x2740cb14851435b5" target="_blank"> 717 University Dr<br>
                            College Station, TX 77840</a></address>
                            <phone>Phone: <a href="tel:+19792607473">(979) 260-7473</a></phone>
                            <a href="http://riseatnorthgate.com" target="_blank">RiseAtNorthgate.com</a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <a href="http://riseatnorthgate.com" target="_blank"><img class="right large-6 medium-12 columns" src="/img/Rise-Logo-Web-Transparent.png"></a>
                        <div class="left large-5 medium-6 columns">
                            <p>At Rise at Northgate, they’ve thought of everything to help you live your best life as a student at Texas A&M. This luxurious and thoughtful community is ideally located adjacent to the main campus for your convenience. You will never have to worry about rush hour, gas prices, or unforeseeable auto-issues because all of your classes are located just steps from your front door. If you are ready to find your ideal student lifestyle in College Station, TX, contact Rise at Northgate today. </p>

                            <p><strong>Community Highlights</strong></p>
                            <ul>
                                <li>Private Bedrooms and Bathrooms</li>
                                <li>Furnished Options Available</li>
                                <li>Resort-Style Amenities</li>
                                <li>A Supportive Atmosphere for Students</li>
                                <li>Adjacent to Texas A&M Main Campus and Northgate District – No Commute!</li>
                                <li>Breathtaking views of College Station</li>
                            </ul>

                            <p><strong>Apartment Amenities</strong></p>
                            <ul>
                                <li>Modern Kitchens with Stainless-Steel Appliances and Sleek Quartz Countertops</li>
                                <li>Open-Air Balconies (in select units) and Bi-Level Penthouse Units</li>
                                <li>Designer Bathrooms with tiled showers and modern fixtures</li>
                                <li>Hardwood-Style Floors and High-End, Energy-Efficient Modern Lighting</li>
                                <li>Plug & Play, Wall-Mounted 42", 46", and 50" Smart TV's</li>
                                <li>Cable & WiFi Included</li>
                            </ul>

                            <p><strong>Community Amenities</strong></p>
                            <ul>
                                <li>Rooftop Sundeck with Infinity Edge Pool/Cabana Lounge/BBQ Grills</li>
                                <li>Skybox Fitness Center/Urban Fitness Studio/Group Fitness Classes</li>
                                <li>High-Tech Study Lounge and Group Meeting Spaces</li>
                                <li>Private Club Room</li>
                                <li>Serenity Spa/Tanning Beds/Steam Room</li>
                                <li>On-Site ATM</li>
                                <li>Bike Storage</li>
                                <li>On-Site, Professional Management</li>
                                <li>State-of-the-Art Security Systems</li>
                                <li>Trash Chutes on Every Floor</li>
                                <li>Private, On-Site Parking</li>
                                <li>Individual Leases</li>
                                <li>Roommate Matching Available</li>
                            </ul>

                        </div>
                        <div class="right large-6 medium-6 columns">
                            <hr>
                            <ul class="clearing-thumbs modal-gallery" data-clearing>
                              <?php
                              for ($i = 1; $i <= 13; $i++) {
                                  echo '<li><a href="img/properties/2016-rise-' . sprintf('%02d', $i) . '.jpg"><img src="img/properties/2016-rise-' . sprintf('%02d', $i) . '-th.jpg"></a></li>';
                              } ?>
                            </ul>
                            <hr>
                                                <div class='embed-container'><iframe src='http://player.vimeo.com/video/100917041?api=1' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
